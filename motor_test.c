#include <stdbool.h>
#include <stdio.h>

#include "brmpuk.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "lcd.h"
#include "sabertooth.h"
#include "serial.h"

int main (void)
{
	/* Control button at PC7. */
	DDRC &= ~(_BV(PC7)|_BV(PC6)|_BV(PC5)|_BV(PC4)|_BV(PC3));
	// pull up:
	PORTC |= (_BV(PC7)|_BV(PC6)|_BV(PC5)|_BV(PC4)|_BV(PC3));

	int bstate = 0;

	motor_init();
	lcd_init();
	sei();

	lcd_text("brm brm");

	signed char m1 = 0, m2 = 0;

	while (1) {
		bool update = false;

		if (bstate != PINC) {
			/* Button state changed. */
			serial_send(bstate);
			bstate = PINC;
		}

		if (serial_waiting()) {
			m1 = serial_recv();
			m2 = serial_recv();
			update = true;
		}

		/* Buttons override serial. */

		if (!(PINC & _BV(PC7))) {
			motor_stop();

		} else if (!(PINC & _BV(PC6))) {
			if (m1 > -63) m1--;
			update = true;

		} else if (!(PINC & _BV(PC5))) {
			if (m1 < 63) m1++;
			update = true;

		} else if (!(PINC & _BV(PC4))) {
			if (m2 > -63) m2--;
			update = true;

		} else if (!(PINC & _BV(PC3))) {
			if (m2 < 63) m2++;
			update = true;
		}

		if (update) {
			motor_send(m1, m2);
			_delay_ms(10);

			char x[40]; snprintf(x, 40, "brm:%d-%d", m1, m2);
			lcd_text(x);
		}
	}

	return 0;
}
