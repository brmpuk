#include "brmpuk.h"

#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include "lcd.h"

static void lcd_putdata(unsigned char data4)
{
	PORTG &= ~0x7;
	PORTG |= data4 & 0x7;

	/* EN pulse: */
	PORTD &= ~_BV(PD7);
	_delay_us(1);
	PORTD |= _BV(PD7);
	_delay_us(1);
}

static void lcd_command(unsigned char data8)
{
	PORTG &= ~_BV(PG4); // RS low
	lcd_putdata(data8 >> 8);
	lcd_putdata(data8);
}

static void lcd_data(unsigned char data8)
{
	PORTG |= _BV(PG4); // RS high
	lcd_putdata(data8 >> 8);
	lcd_putdata(data8);
}

static void lcd_clear(void)
{
	lcd_command(0x01 /* CLEARDISPLAY */);
	_delay_us(2000);
}

void lcd_init(void)
{
	DDRD |= _BV(PD7); // EN pin
	DDRG |= _BV(PG4); // RS pin
	DDRG |= (_BV(PG0)|_BV(PG1)|_BV(PG2)|_BV(PG3)); // data pins
	_delay_us(40000);

	/* Pull RS, EN low to start talking. */
	PORTG &= ~_BV(PG4); // XXX: try pull high?
	PORTD &= ~_BV(PD7);
	_delay_us(4500); // XXX: needed?

	/* Set 4-bit mode. */
	lcd_putdata(0x3);
	_delay_us(4500);
	lcd_putdata(0x3);
	_delay_us(4500);
	lcd_putdata(0x3);
	_delay_us(150);

	lcd_command(0x20 /* FUNCTIONCONTROL */ | 0x08 /* 2LINE */);
	lcd_command(0x08 /* DISPLAYCONTROL */ | 0x04 /* DISPLAYON */);
	lcd_clear();
	lcd_command(0x04 /* ENTRYMORE */ | 0x02 /* LTR */);
}

void lcd_text(char *str)
{
	lcd_clear();
	int i;
	for (i = 0; i < strlen(str); i++) {
		lcd_data(str[i]);
	}
}
