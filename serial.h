#ifndef BRMPUK_SERIAL_H
#define BRMPUK_SERIAL_H

void serial_init(void);

void serial_send(uint8_t serial);

int serial_waiting(void);
uint8_t serial_recv(void);

#endif
