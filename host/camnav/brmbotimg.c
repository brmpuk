/*
 * brmbotimg.c
 *
 * Camera image processing and filtering.
 *
 * gcc -Wall -O3 -ggdb3 -std=gnu99 -o brmbotimg brmbotimg.c apc.c -lm
 */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <linux/videodev2.h>


#define PARITY (1) // 1: blue preference, -1: red preference

#define PUK_THRES 3500 /* 8x8 */
#define CORNER_THRES 200000 /* 40x40 */
#define PUSH_THRES 20000 /* 30x80 */
#define INCORN_THRES 100000 /* 28x28 */

#define CAM_Y_OFS 25

#include "mask.xbm"

static inline bool masked(int x, int y)
{
	int ofs = y * mask_width + x;
	return mask_bits[ofs / 8] & (1 << (7 - ofs % 8));
}

FILE *debugout = NULL;


struct dpix {
	unsigned char y1, u, y2, v;
	//char u, y1, v, y2;
} __attribute__((packed));

/* Negative value: likely red.
 * Positive value: likely blue.
 * Near zero: nothing. */
int assess_pixel(const struct dpix *pixel)
{
	/* TODO */
	return 10*(pixel->u - 128) - 10*(pixel->v - 128); // - (pixel->y1 + pixel->y2)/2 + 138;
}

/* w,h is square radius, not diameter! */
int assess_square(const struct dpix *image, struct v4l2_format *fmt, int x, int y, int w, int h, int parity)
{
	x /= 2; w /= 2; // each dpix is two physical pixels
	int assess = 0;
	int area = h * w;
	int j, i;
	for (j = y - h; j < y + h; j++)
		for (i = x - w; i < x + w; i++)
			if (!masked(i * 2, j)) {
				//const struct dpix *pixel = &image[j * fmt->fmt.pix.width / 2 + i];
				//fprintf(stderr, "%d - %d,%d: Y %d, U %d, V %d\n", assess_pixel(pixel), i*2, j, (pixel->y1 + pixel->y2) / 2, pixel->u, pixel->v);
				int a = assess_pixel(&image[j * fmt->fmt.pix.width / 2 + i]);
				if (__builtin_expect(!parity, 1))
					assess += a;
				else if (a * parity > 0)
					assess += a;
			} else
				area--;
	//return area > 0 ? assess / area : 0;
	return assess;
}

void color_square(struct dpix *image, struct v4l2_format *fmt, int x, int y, int w, int h, int u, int v)
{
	x /= 2; w /= 2; // each dpix is two physical pixels
	int j, i;
	for (j = y - h; j < y + h; j++)
		for (i = x - w; i < x + w; i++) {
			struct dpix *pixel = &image[j * fmt->fmt.pix.width / 2 + i];
			pixel->u = u;
			pixel->v = v;
		}
}

void relative_coords(struct v4l2_format *fmt, int x, int y, int *dx, int *dy)
{
	*dx = (fmt->fmt.pix.width / 2) - x;
	*dy = (fmt->fmt.pix.height / 2 + CAM_Y_OFS) - y;
}
/*       pi/2
 * 0 --- * --- +-pi
 *      -pi/2
 */
double angle_of(int dx, int dy)
{
	double m = 1.0f, a = 0.0f;
	if (dx < 0) dx = -dx, a = M_PI, m = -m;
	if (dy < 0) dy = -dy, m = -m;
	return m * (a + atan((double) dy / dx));
}
double weight_distance(int dx, int dy)
{
	return pow((1 - 0.002), sqrt(dx*dx + dy*dy));
}

#if 0
/* Scan unmasked portion of image for blobs of given parity.
 * Consider their relative positions, producing angle in the average
 * blob direction:
 */
double blob_scan(struct dpix *image, struct v4l2_format *fmt, int parity)
{
	double avg_angle = 0;
	int avg_weight = 100; // tend to go straight ahead

	int square_radius = 4;
	int assess_any_thres = 5000;
	for (int x = square_radius; x < fmt->fmt.pix.width; x += square_radius * 2)
		for (int y = square_radius; y < fmt->fmt.pix.height; y += square_radius * 2) {
			int assess = assess_square(image, fmt, x, y, square_radius, square_radius, 0);
			if (abs(assess) < assess_any_thres) {
				color_square(image, fmt, x, y, square_radius, square_radius, 0, 0);
				continue;
			}
			assess *= parity;
			if (assess < 0) {
				color_square(image, fmt, x, y, square_radius, square_radius, 255, 255);
				continue;
			}
			int dx, dy;
			relative_coords(fmt, x, y, &dx, &dy);
			double angle = angle_of(dx, dy);
			double weight = assess * weight_distance(dx, dy);

			fprintf(stderr, "%d,%f blob @ %d,%d (%d,%d) angle %f\n", assess, weight, x, y, dx, dy, angle);

			avg_angle += (angle - avg_angle) * weight / avg_weight;
			avg_weight += weight;
		}

	fprintf(stderr, "total angle %f [%d]\n", avg_angle, avg_weight);
	return avg_angle;
}
#endif

/* Scan unmasked portion of image for blobs of given parity.
 * Find direction and distance of the nearest one. */
void nearest_blob_scan(struct dpix *image, struct v4l2_format *fmt, int parity, double *best_angle, double *best_dist)
{
	*best_angle = NAN;
	*best_dist = 100000;

	int square_radius = 4;
	for (int x = square_radius; x < fmt->fmt.pix.width; x += square_radius * 2)
		for (int y = square_radius; y < fmt->fmt.pix.height; y += square_radius * 2) {
			int assess = assess_square(image, fmt, x, y, square_radius, square_radius, 0);
			if (abs(assess) < PUK_THRES) {
				continue;
			}
			assess *= parity;
			if (assess < 0) {
				color_square(image, fmt, x, y, square_radius, square_radius, 128, 255);
				continue;
			}
			color_square(image, fmt, x, y, square_radius, square_radius, 128, 0);
			int dx, dy;
			relative_coords(fmt, x, y, &dx, &dy);
			double angle = angle_of(dx, dy);

			fprintf(stderr, "%d>%d blob @ %d,%d (%d,%d) angle %f\n", assess, PUK_THRES, x, y, dx, dy, angle);

			double dist2 = dx * dx + dy * dy;
			if (dist2 < *best_dist) {
				*best_angle = angle;
				*best_dist = dist2;
			}
		}

	fprintf(stderr, "best blob angle %f dist %f\n", *best_angle, *best_dist);
	*best_dist = sqrt(*best_dist);
}

int color_neighbor_blob_squares(struct dpix *image, struct v4l2_format *fmt, int parity, int kx, int ky, int base_radius, int dist)
{
	int square_radius = 4;
	int edgel = dist + base_radius / square_radius;
	int qx[4 * edgel], qy[4 * edgel];
	int sx = kx - base_radius - dist * square_radius;
	int sy = ky - base_radius - dist * square_radius;
	int mx = kx + base_radius + dist * square_radius;
	int my = ky + base_radius + dist * square_radius;
	int x = sx, y = sy;
	int i = 0;
	int taboo = 0;

	while (x < mx) { qx[i] = x; qy[i] = y; x += square_radius * 2; i++; }
	while (y < my) { qx[i] = x; qy[i] = y; y += square_radius * 2; i++; }
	while (x > sx) { qx[i] = x; qy[i] = y; x -= square_radius * 2; i++; }
	while (y > sy) { qx[i] = x; qy[i] = y; y -= square_radius * 2; i++; }

	for (i = 0; i < 4 * edgel; i++) {
		int x = qx[i], y = qy[i];
		if (x < square_radius || x > fmt->fmt.pix.width - square_radius) continue;
		if (y < square_radius || y > fmt->fmt.pix.height - square_radius) continue;

		int assess = assess_square(image, fmt, x, y, square_radius, square_radius, 0) * parity;
		if (assess < PUK_THRES)
			continue;
		// puk-like square near the corner is taboo!
		color_square(image, fmt, x, y, square_radius, square_radius, 0, 0);
		taboo++;
	}
	return taboo;
}

double corner_angle(struct dpix *image, struct v4l2_format *fmt, int parity)
{
	int square_radius = 20;
	for (int x = square_radius; x < fmt->fmt.pix.width; x += square_radius * 2)
		for (int y = square_radius; y < fmt->fmt.pix.height; y += square_radius * 2) {
			int assess = assess_square(image, fmt,x, y, square_radius, square_radius, 0);
			if (assess * parity > CORNER_THRES) {
				/* Color the square! This way, we will not confuse it with blobs. */
				color_square(image, fmt, x, y, square_radius, square_radius, 0, 0);
				int dx, dy;
				relative_coords(fmt, x, y, &dx, &dy);
				double angle = angle_of(dx, dy);
				fprintf(stderr, "%d>%d corner @ %d,%d (%d,%d) angle %f\n", assess, CORNER_THRES, x, y, dx, dy, angle);
				for (int dist = 0; color_neighbor_blob_squares(image, fmt, parity, x, y, square_radius, dist); dist++);
				return angle;
			}
		}
	fprintf(stderr, "not seeing corner!\n");
	return NAN;
}

bool pushing(struct dpix *image, struct v4l2_format *fmt, int parity)
{
	int x = 250, y = 240 + CAM_Y_OFS, xr = 15, yr = 40;
	double value = assess_square(image, fmt, 250, 240 + CAM_Y_OFS, 15, 40, parity) * parity;
	color_square(image, fmt, x, y, xr, yr, 0, 0);
	fprintf(stderr, "Pushing check: %d,%d %dx%d %f >? %d\n", x, y, xr, yr, value, PUSH_THRES);
	return value > PUSH_THRES;
}

bool in_corner(const struct dpix *image, struct v4l2_format *fmt, int parity)
{
	double value = assess_square(image, fmt, 220, 240 + CAM_Y_OFS, 14, 14, parity) * parity;
	fprintf(stderr, "In-corner check: 220,260 14x14 %f >? %d\n", value, INCORN_THRES);
	return value > INCORN_THRES;
}

double randomize_angle(double angle)
{
#define ANGLE_SPREAD (M_PI/5)
	return angle - ANGLE_SPREAD/2 + ((double)random() / RAND_MAX * ANGLE_SPREAD);
}


void process_image(void *p, int size /* 614400 */, struct v4l2_format *fmt)
{
#if 0
	static FILE *f;
	if (!f) f = fopen("stream.raw", "wb");
	fwrite(p,size,1,f);
#endif

	struct timeval tv;
	gettimeofday(&tv, NULL);
	static int framei;
	fprintf(stderr, "\n----------------------------------------------------------------------\n"
		"\n[%d %ld,%ld] frame (size %d w %d h %d bpl %d)\n",
		framei, tv.tv_sec, tv.tv_usec,
		size, fmt->fmt.pix.width, fmt->fmt.pix.height, fmt->fmt.pix.bytesperline);
	framei++;

	/* Perhaps we have a goal? */
	static int goal_timer = 0;
	if (goal_timer > 0) {
		/* In that case, do not override existing orders. */
		goal_timer--;
		printf("TICK\n");
		goto next; fflush(stderr);
	}

	/* Each two bytes are (Y, U, Y, V) representing two
	 * adjecent pixels. */
	struct dpix *image = p;

	/* Camera image:
	 *
	 *       RIGHT
	 * FRONT       REAR
	 *       LEFT
	 */

	/* Get basic navigation data - are we pushing and where is our home? */
	bool are_in_corner = in_corner(image, fmt, PARITY);
	double corner = corner_angle(image, fmt, PARITY);
	bool are_pushing = pushing(image, fmt, PARITY);

	double angle = 0;
	bool backoff = false;

	/* First, test for emergency back-off. If, for the last N frames,
	 * the corner position has not changed (and is not ahead), back off
	 * in random (narrow) angle. */
	if (!isnan(corner) && abs(corner) > 0.1) {
#define FRAME_TEST 12
		static double corners[FRAME_TEST];
		static int corners_k = 0;
		corners[corners_k++] = corner;
		if (corners_k >= FRAME_TEST)
			corners_k = 0;

		for (int i = 0; i < FRAME_TEST; i++)
			if (abs(corners[i] - corner) > 0.1)
				goto no_backoff;
		angle = randomize_angle(M_PI);
		backoff = 1;
		goal_timer = 15;
		fprintf(stderr, "Emergency backoff! Corner stays at %f\n", corner);
		goto go;
no_backoff:;
	}

	if (are_in_corner) {
		/* Escape! If we know what to get away from, do that.
		 * Otherwise go in a random angle for a second. */
		if (!isnan(corner) && random() % 100 > 10) {
			fprintf(stderr, "Corner escape: %f\n", corner);
			angle = randomize_angle(corner);
			fprintf(stderr, "-> %f\n", angle);
		} else {
			fprintf(stderr, "Corner escape: at random\n");
			angle = (double)random() / RAND_MAX * M_PI * 2 - M_PI;
			fprintf(stderr, "-> %f\n", angle);
		}
		backoff = 1;
		goal_timer = 25;

	} else if (!isnan(corner) && are_pushing) {
		/* Go to corner. */
		fprintf(stderr, "Pushing to corner: %f\n", corner);
		angle = corner;
		goal_timer = 10;

	} else {
		/* Look for blobs. */
		double dist;
		nearest_blob_scan(image, fmt, PARITY, &angle, &dist);
		if (!isnan(angle)) {
			/* Cool! */
			goal_timer = 10;
#if 0
			/* If we are closing in and know where the
			 * corner is, start evasion sequence so that
			 * we, the blob and the corner are lined up
			 * properly when we start pushing it. */
			if (dist < 150 && !isnan(corner)) {
				if (abs(angle - corner) > M_PI/2) {
					/* Go around the blob. */
					angle = angle + M_PI/4 * (random() % 2 ? 1 : -1);
					fprintf(stderr, "evasion sequence: %f\n", angle);
					goal_timer = 20;
				}
			}
#endif

		} else {
			/* We are lost! Tilt around for a bit. */
			angle = randomize_angle(M_PI);
			goal_timer = 20;
		}
	}

go:
	if (!goal_timer) goal_timer = 3; // default decision granularity
	if (angle > M_PI) angle -= 2*M_PI;
	fprintf(stderr, "Final angle (%d) %f, following for %d ticks\n", backoff, (angle * 180 / M_PI), goal_timer);
	printf("%s %d\n", backoff ? "BACKOFF" : "ANGLE", ((int) (angle * 180 / M_PI)));
	fflush(stdout);
	fflush(stderr);

next:
	if (!debugout) debugout = fopen("out.raw", "wb");
	// comment out below for no debugging
	fwrite((char*) p, size, 1, debugout);
}

#if 0
int main(void)
{
	struct v4l2_format fmt = { .fmt = { .pix = { .width = 640, .height = 480, .bytesperline = 1280 } } };
	char frame[614400];
	while (!feof(stdin)) {
		int i = fread(frame, 614400, 1, stdin);
		if (i < 1) break;
		process_image(frame, 614400, &fmt);
	}
	return 0;
}
#endif
