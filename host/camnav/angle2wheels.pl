#!/usr/bin/perl -l

use warnings;
use strict;
use Math::Trig;
use POSIX;
use Time::HiRes qw(usleep);

$| = 1;

my $basespeed = 15;
my $minspeed = 5;
my $maxspeed = 30;
my ($a, $b) = ($basespeed, $basespeed);

#my $accel = 1.2;
#my $deccel = 0.4;
my $accel = 3;
my $deccel = 0.2;
my $odir = 1;
my $dir = 1;

sub timid {
	my ($old, $new) = @_;
	# limit acceleration
	if ($new > $old * $accel) {
		$new = $old * $accel;
	} elsif ($new < $old * $deccel) {
		$new = $old * $deccel;
	}
	# make turns more pronounced
	return ceil($new);
}

while (<>) {
	chomp;
	@_ = split(/\s+/);
	if ($_[0] eq 'ANGLE') {
		$dir = 1;
	} elsif ($_[0] eq 'BACKOFF') {
		$dir = -1;
	} else {
		goto adjust;
	}
	$_[1] =~ /^[-\d]+$/ or next;
	$_[1] *= 150; $_[1] /= 100;

	if ($_[1] > 0) {
		$_[1] = 75 if ($_[1] > 75);
		$b = timid($b, $basespeed);
		$a = timid($a, $b / cos(-$_[1] * pi / 180));
	} else {
		$_[1] = -75 if ($_[1] < -75);
		$a = timid($a, $basespeed);
		$b = timid($b, $a / cos(-$_[1] * pi / 180));
	}

	# Make sure we do not go too fast - in that case, slow down
	# the other wheel proportionally.
	if ($a > $maxspeed) {
		$b -= $a - $maxspeed;
		$a = $maxspeed;
	}
	if ($b > $maxspeed) {
		$a -= $b - $maxspeed;
		$b = $maxspeed;
	}
	if ($a < $basespeed and $b < $basespeed) {
		$a++, $b++;
	}
	if ($a < $minspeed) {
		$a = $minspeed;
	}
	if ($b < $minspeed) {
		$b = $minspeed;
	}

adjust:
	if ($dir ne $odir) {
		# Skip a turn by stopping.
		print STDERR "stop";
		print "0 0";
		usleep 500000;
		$odir = $dir;
		$_[1] = 'tick';
	} elsif (not $_[1]) {
		next;
	}

	print STDERR join(' ', $_[1], $dir * $a, $dir * $b);
	print join(' ', $dir * $a, $dir * $b);
}
