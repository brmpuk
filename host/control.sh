#!/bin/sh
stty -F /dev/ttyUSB0 38400 parodd -cstopb
while read a b; do
	[ $a -ge 0 ] || a=$((256+a))
	[ $b -ge 0 ] || b=$((256+b))
	echo -ne "\x$(printf "%x" $a)\x$(printf "%x" $b)" >/dev/ttyUSB0
done
