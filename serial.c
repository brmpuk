#include "brmpuk.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include "serial.h"


#define NOINT
#ifndef NOINT

/* Ring buffer for received data. */

#define BUF_SIZE 64

struct ring_buffer {
	/* ........incoming.......
	 *         ^head   ^head+bytes */
	unsigned char buffer[BUF_SIZE];
	int head;
	int bytes;
} buf = { { 0 }, 0, 0 };

static inline void store_char(uint8_t data)
{
	if (buf.bytes >= BUF_SIZE)
		return; // overflow!

	int i = (buf.head + buf.bytes) % BUF_SIZE;
	buf.buffer[i] = data;
	buf.bytes++;
}

#endif


/* USART hardware handling. */

#define BAUD 38400  // Rychlost prenosu dat
#define MYUBRR (F_CPU/16/BAUD-1) // Konstanta pouzita pro nastaveni prenosu

static void USART0_Init(uint16_t ubrr) { // fukce pro inicializaci USART0 komunikace 
	UBRR0H = (uint8_t) (ubrr >> 8);
	UBRR0L = (uint8_t) ubrr;

	UCSR0C = (1<<UCSZ01)|(1<<UCSZ00) | (1<<UPM01)|(1<<UPM00)/* | (1<<USBS0)*/; // 8 N 1
	UCSR0A = 0; // no special stuff
	
	UCSR0B = (1<<RXEN0)|(1<<TXEN0)
#ifndef NOINT
		| (1<<RXCIE0)
#endif
		;
}

static void USART0_Transmit(uint8_t data) { // poslani dat po USART0
 	while (!(UCSR0A & (1<<UDRE0))) ; // cekani na odesilaci buffer
	UDR0 = data; // zapsani dat = odeslani dat po USART0
}

#ifndef NOINT
SIGNAL(SIG_UART0_RECV)
{
	unsigned char data = UDR0;
	store_char(data);
	if (data >= 'A' && data <= 'Z')
		data ^= 0x20;
	USART0_Transmit(data);
}
#endif


/* Public API. */

void serial_init(void)
{
	USART0_Init(MYUBRR);
}

void serial_send(uint8_t data)
{
	USART0_Transmit(data);
}

int serial_waiting(void)
{
#ifdef NOINT
	return UCSR0A & _BV(RXC0);
#else
	return buf.bytes;
#endif
}

uint8_t serial_recv(void)
{
#ifdef NOINT
	volatile int x = 0;
	while (!serial_waiting()) x++;

	uint8_t data = UDR0;
#if 0
	if (data >= 'A' && data <= 'Z')
		data ^= 0x20;
	USART0_Transmit(data);
#endif
	return data;
#else
	// cli-sti is actually not required, this is race-free!
	uint8_t data = buf.buffer[buf.head];
	buf.bytes--;
	buf.head = (buf.head + 1) % BUF_SIZE;
	return data;
#endif
}
