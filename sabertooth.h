#ifndef BRMPUK_SABERTOOTH_H
#define BRMPUK_SABERTOOTH_H

void motor_init(void);

/* m1, m2 are in range [-63, +63] */
void motor_send(signed char m1, signed char m2);

void motor_stop(void);

#endif
