#include "brmpuk.h"

#include <avr/io.h>
#include "sabertooth.h"

#define BAUD 38400  // Rychlost prenosu dat
#define MYUBRR (F_CPU/16/BAUD-1) // Konstanta pouzita pro nastaveni prenosu

static void USART1_Init(uint16_t ubrr) { // fukce pro inicializaci USART1 komunikace 
	UBRR1H = (uint8_t) (ubrr >> 8);
	UBRR1L = (uint8_t) ubrr;
	
	UCSR1B = (1<<RXEN1)|(1<<TXEN1); 
	UCSR1C = (1<<UCSZ11)|(1<<UCSZ10); // nastaveni stop bitu a pocet bitu zpravy 8bit a 1stop bit
}

static void USART1_Transmit(uint8_t data) { // poslani dat po USART1
 	while (!(UCSR1A & (1<<UDRE1))) ; // cekani na odesilaci buffer
	UDR1 = data; // zapsani dat = odeslani dat po USART1
}

void motor_init(void)
{
	USART1_Init(MYUBRR);
	motor_stop();
}

void motor_send(signed char m1, signed char m2)
{
     	USART1_Transmit(~0x80 & (64 - m1));
     	USART1_Transmit(0x80 | (64 - m2));
}

void motor_stop(void)
{
	USART1_Transmit(0);
}
