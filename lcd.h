#ifndef BRMPUK_LCD_H
#define BRMPUK_LCD_H

void lcd_init(void);
void lcd_text(char *str);

#endif
